from bs4 import BeautifulSoup

def clean_up(soup):
    for tag in soup.find_all():   
        try:
            nattrs = { }
            if "href" in tag.attrs.keys():
                nattrs['href'] = tag.attrs['href']
            tag.attrs = nattrs
            
            if len(tag.contents) == 0:
                tag.decompose()
        except AttributeError: 
            # 'NavigableString' object has no attribute 'attrs'
            pass

def clean_id(s):
    s = s.replace("http://fb.me/", "").replace("/", "")
    s = s.replace(",", "").replace(" ", "")
    return s

def append_links(c, link):
    if not 'links' in c:
        c['links'] = []
        
    c['links'].append(link)
    c['links'] = list(set(c['links']))

def find_id(t):    
    for a in t.find_all('a'):
        if a.string:
            if "fb.me" in a.string:
                return clean_id(a.string)
            
    for a in t.find_all('a'):
        if a.string:
            return clean_id(a.string)

    for a in t.strings:
        return clean_id(a)

    raise ValueError("ID not found " + str(t))

def parse(config):
    ##
    print ("parsing florida data")

    floridadata = config.datadir + "florida/state-sub-chapters"

    print ("opening data " + floridadata)

    with open(floridadata) as fp:
        soup = BeautifulSoup(fp, "html.parser")

        clean_up(soup)
        
        for sec in soup.body.find_all('section'):
            h2s = sec.find_all('h2')
            if len(h2s)>0:
                # regional branch
                p = h2s[0].parent
                
                regional = {}
                regionalsub = {}
                
                for info in p:
                    if(info.name == "h2"):
                        regional = parse_regional(config, info)
                        print("regional parsed " + regional['id'])
                    elif(info.name == "h3"):
                        regionalsub = parse_regionalsub(config, info, regional)
                    elif(info.name == "ul"):
                        for sublink in info.find_all('a'):
                            if not sublink.string in regionalsub['links']:
                                regionalsub['links'].append(sublink.string)
                                print("regional_sub: " + str(regionalsub))

def parse_regionalsub_content(config, info, regional, sub):
    subdata = config.datadir + "florida/" + sub['subname']
    print ("parse_regionalsub_content subdata:" + subdata)
    
    with open(subdata) as fp:
        soup = BeautifulSoup(fp, "html.parser")
        clean_up(soup)
        
        content = soup.body.find_all('h2')[0].parent
        for pinfo in content.contents:
            print("parse_regionalsub_content pinfo " + str(pinfo))
            
            if len(pinfo.parent.contents) > 2:
                info = pinfo.parent
                name = ""
                for c in info.contents:
                    if c.name == "h1":
                        for n in c.find_all():
                            for c in n.strings:
                                name += str(c)
                
                name = name.strip()
                if "" != name: 
                    print ("name:" + name)
                    sub['name'] = name

        for link in content.find_all('a'):
            if link.string and not link.string in sub['links']:
                print("parse_regionalsub_content " + sub['id'] + " appending " + str(link.string))
                sub['links'].append(link.string)

def parse_regionalsub(config, info, regional):
    print ("h3 " + str(info.find_all()))

    name = ""
    link = ""
    
    for sub in info.div.contents:
        print ("sub " + str(sub))

        if sub.name == "a":
            name += sub.string
            if "href" in sub.attrs:
                link = sub.attrs['href']
        elif sub.name != "div":
            if str(sub) != ":  ":
                name += str(sub).rstrip() + " "
 
    regionalsub = config.db.get_branch(find_id(info))
    regionalsub['name'] = name.rstrip()
    
    link = link.strip()
    if link != "":
        regionalsub['subname'] = link
    
    if not 'links' in regionalsub:
        regionalsub['links'] = []

    print ("sub: " + str(regionalsub))
    
    if 'subname' in regionalsub:
        parse_regionalsub_content(config, info, regional, regionalsub)

    print ("content parsed sub: " + str(regionalsub))
    
    return regionalsub

def parse_regional(config, info):
    print ("h2 " + str(info.div.contents))
    # regional chapter title
        
    name = ""
    fb_url = ""
    
    for rega in info.div.contents:
        if rega.name == "a":
            href = rega.attrs['href']
            if not "http" in href:
                name += rega.string
            else:
                fb_url = rega.string
        elif rega.name != "div":
            if str(rega) != ":  ":
                name += str(rega).replace(":", "").rstrip() + " "
    

    regional = config.db.get_branch(find_id(info))
    regional['name'] = name.rstrip()
    append_links(regional, fb_url)

    print ("regional parsed: " + regional['id'])
    print ("regional: " + str(regional))
    return regional
