from bs4 import BeautifulSoup
from bs4.element import Tag

def parse_branch(config, name):
    print ("parsing GCT data - " + name)

    data = config.datadir + "gct.zeitgeist-info.com/" + name
    with open(data) as fp:
        soup = BeautifulSoup(fp, "html.parser")
        
        ch = config.db.get_branch(name)
        
        c = soup.find_all('h3')[0].parent
        
        valuename = None

        print("content " + str(c))
        for line in c.contents:
            if ":" in line:
                line = line.strip()
                print ("line " + str(line))
                splitted = line.split(":")
                valuename = splitted[0].strip()
                value = str(splitted[1]).strip()
                if len(value) > 0:
                    print ("setting value " + splitted[0].strip() + " to " + value)
                    ch[valuename] = value
                    valuename = None
            elif valuename != None:
                if type(line) == Tag and len(line.attrs)>0:
                    link = line
                    href = link.attrs['href']
                    print("linkvalue : " + href)
                    ch[valuename] = href
                    valuename = None

def parse(config):
    ##
    print ("parsing GCT data")

    gctdata = config.datadir + "gct.zeitgeist-info.com/directory"

    with open(gctdata) as fp:
        soup = BeautifulSoup(fp, "html.parser")

#        for tbody in soup.find_all("tbody"):
#            #print("tbody " + str(tbody))

        for tr in soup.find_all("tr"):
            #print("tr " + str(tr))
            
            for a in tr.find_all('a'):
                branchlink = a.attrs['href']
                print("branches " + str(branchlink))
                parse_branch(config, str(branchlink))

    print("GCT data done")

