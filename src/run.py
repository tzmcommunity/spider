from parsers import florida
from parsers import gct
from tasks import feeds
import config

florida.parse(config)
gct.parse(config)
feeds.run(config)

config.write_db()
