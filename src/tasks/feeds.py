import httplib2
import sys
from datetime import datetime, timedelta

dateformat = '%Y-%m-%d %H:%M:%S'

def update_feed(config, b):
    website = b['Website']

    print ("Trying to find branch feed. Org website: " + website)

    website = fix_website_url(website)
    b['Website'] = website

    
    try:
        h = httplib2.Http()
        resp = h.request(website, 'HEAD')
        b['website.working'] = "true"
    except:
        e = sys.exc_info()[0]
        b['website.working'] = "false"
        print ("Failed to connect to site " + website + " e:" + str(e)) 

    if b['website.working'] == "true":
        feeds = ['feed.xml', 'rss.xml', 'rss/', 'site/rss/']
        for f in feeds:
            feedurl = website + f
            print ("Trying feed URL " + feedurl)
            h = httplib2.Http()
            resp = h.request(feedurl, 'HEAD')[0]
            print("feed ok? " + str(resp))
            print("feed ok? " + str(resp.status))
            
            if resp['status'] == 200:
                print ("Feed found " + feedurl)
                b['website.feed.url'] = feedurl  
                break;

    b['website.tested.datetime'] = str(datetime.now().strftime(dateformat))

def fix_website_url(website):
    if not "http" in website:
        website = "https://" + website 

    if "http:" in website:
        website = website.replace("http://", "https://")

    if website[-1] != "/":
        website += "/"

    print ("Website URL: " + website)
    return website

def should_try_to_find_feed(website, b):
    if website is None: 
        return False
    if website=='N/A': 
        return False
    
    if 'website.tested.datetime' not in b:
        return True

    last_hour_date_time = datetime.now() - timedelta(days = 1)
    tested = datetime.strptime(b['website.tested.datetime'], dateformat)
    print ("last hour: " + last_hour_date_time.strftime(dateformat) + " tested:" + str(tested))
    
    if b['website.working']=='false' and tested > last_hour_date_time:
        print ("Not trying again " + website)
        return False
    
    if 'website.feed.url' in b: 
        return False
    
    print ("Should try to find feed URL " + website)
    return True

def run(config):
    ##
    print ("Finding site feeds")
    
    for branchname in config.db.get_branches():
        branch = config.db.get_branch(branchname)
        print("########### Branch " + branchname)
        print("Branch " + str(branch))
        if 'Website' in branch:
            website = branch['Website']
            print("Branch website " + str(website) + " type: " + str(type(website)))
            if should_try_to_find_feed(website, branch):            
                update_feed(config, branch)

