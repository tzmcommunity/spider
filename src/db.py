import json

data = {
}

def load_db(path):
    global data
    
    print ("loading db")
    
    try:
        with open(path, "r") as read_file:
            data = json.load(read_file)
    except FileNotFoundError:
        print("File not found. Creating a new database")
    
    if not 'branches' in data.keys():
        data['branches'] = {}

def get_branches():
    return data['branches']

def get_branch(nbid):
    bid = str(nbid)
    
    if bid in data['branches']:
        return data['branches'][bid]
    else:
        b = {
            "id": bid
        }
        data['branches'][bid] = b
        return b

def write_to(path):
    print("writing db to " + path)
    with open(path, "w") as write_file:
        json.dump(data, write_file, sort_keys=True, indent=4)
