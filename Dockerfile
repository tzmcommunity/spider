FROM python

RUN python -V
RUN echo 20200413 1301

RUN mkdir -p /opt/data/target

# ls in the just to make sure loading succeeded
RUN cd /opt/data && wget -np -r -m https://gct.zeitgeist-info.com || true && ls  /opt/data/gct.zeitgeist-info.com/ch-usa
RUN cd /opt/data && wget -np -r -m --convert-links --no-directories --directory-prefix=florida https://sites.google.com/view/zeitgeistmovementflorida/state-sub-chapters || true && ls /opt/data/florida/state-sub-chapters

RUN cat /opt/data/gct.zeitgeist-info.com/directory

RUN find /opt/data/

RUN pip install BeautifulSoup4
RUN pip install httplib2

RUN find /opt/data/
RUN ls -l /opt/data/florida/state-sub-chapters

ADD src /opt/src

#ENTRYPOINT ping localhost
ENTRYPOINT cd /opt/src/ && python run.py
