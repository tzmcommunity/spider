#!/bin/bash

docker build -t tzm.community.spider .

for d in $(docker ps | grep zm.community.spider | awk -S '{ print $1; }'); do
	echo "container ${d}"
	docker ps | grep ${d}
	docker kill ${d} || true
	docker rm ${d} || true	
done

docker run  --rm -v ~/git/data/spider:/opt/data/target --name tzm.community.spider tzm.community.spider

cp ~/git/data/spider/data.json ~/git/chapter.site.root/spider.json
